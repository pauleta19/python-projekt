from Game.Bricks import Brick
from Game.Share import *


class LifeAddBrick(Brick):

    def __init__(self, pos, image, game):
        super(LifeAddBrick, self).__init__(pos, image, game)

    def hit(self):
        game = self.getGame()
        game.increaseLives()

        super(LifeAddBrick, self).hit()

    def getHitSound(self):
        return Constants.SOUND_HIT_BRICK_ADD_LIFE
