from Game.Share import GameObject
from Game.Share import Constants


class Brick(GameObject):

    def __init__(self, pos, image, game):
        self.__game = game
        self.__hitPoints = 100
        self.__lives = 1
        super(Brick, self).__init__(pos, Constants.BRICK_SIZE, image)

    def getGame(self):
        return self.__game

    def isDestroyed(self):
        return self.__lives <= 0

    def getHitPoints(self):
        return self.__hitPoints

    def hit(self):
        self.__lives -= 1

    def getHitSound(self):
        return Constants.SOUND_HIT_BRICK

