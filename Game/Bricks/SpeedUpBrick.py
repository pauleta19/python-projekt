from Game.Bricks import Brick
from Game.Share import *


class SpeedUpBrick(Brick):

    def __init__(self, pos, image, game):
        super(SpeedUpBrick, self).__init__(pos, image, game)

    def hit(self):
        game = self.getGame()

        for ball in game.getBalls():
            ball.setSpeed(ball.getSpeed() + 1)

        super(SpeedUpBrick, self).hit()

    def getHitSound(self):
        return Constants.SOUND_HIT_BRICK_SPEED_UP

