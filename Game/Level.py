import os
import fileinput
import pygame
import random

from Game.Bricks import *
from Game.Share.Constants import Constants
class Level:

    def __init__(self, game):
        self.__game = game
        self.__bricks = []
        self.__amountOfBricksLeft = 0
        self.__currentLevel = 0

    def getBricks(self):
        return self.__bricks

    def getAmountOfBricksLeft(self):
        return self.__amountOfBricksLeft

    def brickHit(self):
        self.__amountOfBricksLeft -= 1

    def loadNextLevel(self):
        self.__currentLevel += 1
        fileName = os.path.join("Game", "Resources", "Levels", "level" + str(self.__currentLevel) + ".dat")

        if not os.path.exists(fileName):
            self.loadRandom()

        else:
            self.load(self.__currentLevel)

    def loadRandom(self):
        self.__bricks = []

        x, y = 0, 0

        maxBricks = int(Constants.SCREEN_SIZE[0] / Constants.BALL_SIZE[0])
        rows = random.randint(2, 8)

        amountOfSpecialBricks = 0
        for row in range(0, rows):
            for brick in range(0, maxBricks):
                brickType = random.randint(0, 3)
                if brickType == 1 or amountOfSpecialBricks >= 2:
                    brick = Brick([x, y], pygame.image.load(Constants.IMAGE_BRICK), self.__game)
                    self.__bricks.append(brick)
                    self.__amountOfBricksLeft += 1


                elif brickType == 2:
                    brick = SpeedUpBrick([x, y], pygame.image.load(Constants.IMAGE_SPEED_UP_BRICK), self.__game)
                    self.__bricks.append(brick)
                    self.__amountOfBricksLeft += 1
                    amountOfSpecialBricks += 1

                elif brickType == 3:
                    brick = LifeAddBrick([x, y], pygame.image.load(Constants.IMAGE_LIFE_UP_BRICK), self.__game)
                    self.__bricks.append(brick)
                    self.__amountOfBricksLeft += 1
                    amountOfSpecialBricks += 1


                x += Constants.BRICK_SIZE[0]

            x = 0
            y += Constants.BRICK_SIZE[1]

    def load(self, level):
        self.__currentLevel = level
        self.__bricks = []

        x, y = 0, 0

        for line in fileinput.input(os.path.join("Game", "Resources", "Levels", "level" + str(level) + ".dat")):
            for currentBrick in line:
                if currentBrick == "1":
                    brick = Brick([x, y], pygame.image.load(Constants.IMAGE_BRICK), self.__game)
                    self.__bricks.append(brick)
                    self.__amountOfBricksLeft += 1


                elif currentBrick == "2":
                    brick = SpeedUpBrick([x, y], pygame.image.load(Constants.IMAGE_SPEED_UP_BRICK), self.__game)
                    self.__bricks.append(brick)
                    self.__amountOfBricksLeft += 1


                elif currentBrick == "3":
                    brick = LifeAddBrick([x, y], pygame.image.load(Constants.IMAGE_LIFE_UP_BRICK), self.__game)
                    self.__bricks.append(brick)
                    self.__amountOfBricksLeft += 1


                x += Constants.BRICK_SIZE[0]

            x = 0
            y += Constants.BRICK_SIZE[1]




