import pygame
from Game.Views.View import View
from Game.Share import *

class PlayingGameView(View):

    def __init__(self, game):
         super(PlayingGameView, self).__init__(game)

    def render(self):
        super(PlayingGameView, self).render()

        game = self.getGame()
        level = game.getLevel()
        balls = game.getBalls()

        if level.getAmountOfBricksLeft() <= 0:
            for ball in balls:
                ball.setMotion(0)

            level.loadNextLevel()

        if game.getLives() <= 0:
            game.playSound(Constants.SOUND_GAMEOVER)
            game.changeView(Constants.GAMEOVER_VIEW)

        paddle = game.getPaddle()
        for ball in balls:
            for ball2 in balls:
                if ball != ball2 and ball.intersects(ball2):
                    ball.changeDirection(ball2)

            for brick in game.getLevel().getBricks():
                if not brick.isDestroyed() and ball.intersects(brick):
                    game.playSound(brick.getHitSound())
                    brick.hit()
                    level.brickHit()
                    game.increaseScore(brick.getHitPoints())
                    ball.changeDirection(brick)
                    break

            if ball.intersects(paddle):
                game.playSound(Constants.SOUND_HIT_PADDLE)
                ball.changeDirection(paddle)

            ball.updatePos()

            if ball.isBallDead():
                ball.setMotion(0)
                game.reduceLives()

            game.screen.blit(ball.getImage(), ball.getPos())

        for brick in game.getLevel().getBricks():
            if not brick.isDestroyed():
                game.screen.blit(brick.getImage(), brick.getPos())


        paddle.setPos((pygame.mouse.get_pos()[0], paddle.getPos()[1]))
        game.screen.blit(paddle.getImage(), paddle.getPos())

        self.clearText()

        self.addText("Wynik: " + str(game.getScore()),
                     x = 0,
                     y = Constants.SCREEN_SIZE[1] - 60, size = 28)


        self.addText("Zycia: " + str(game.getLives()),
                     x = 0,
                     y = Constants.SCREEN_SIZE[1] - 30, size = 28)

    def handleEvents(self, events):
        super(PlayingGameView, self).handleEvents(events)

        for event in events:
            if event.type == pygame.QUIT:
                exit()

            if event.type == pygame.MOUSEBUTTONDOWN:
                for ball in self.getGame().getBalls():
                    ball.setMotion(1)