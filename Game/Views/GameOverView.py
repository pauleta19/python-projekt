import pygame
from Game.Views.View import View
from Game.Share import *
from Game import Highscores

class GameOverView(View):

    def __init__(self, game):
        super(GameOverView, self).__init__(game)

        self.__playerName = ""
        self.__highscoresImage = pygame.image.load(Constants.IMAGE_HIGHSCORES)

    def render(self):

        self.getGame().screen.blit(self.__highscoresImage, (50, 50))

        self.clearText()
        self.addText("Twoje imie: ", 310, 200, size=40)
        self.addText(self.__playerName, 460, 200, size=40)
        super(GameOverView, self).render()

    def handleEvents(self, events):
        super(GameOverView, self).handleEvents(events)

        for event in events:
            if event.type == pygame.QUIT:
                exit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    game = self.getGame()
                    Highscores().add(self.__playerName, game.getScore())
                    game.reset()
                    game.changeView(Constants.HIGHSCORES_VIEW)
                elif event.key >= 65 and event.key <= 122:
                    self.__playerName += chr(event.key)
                if event.key == pygame.K_F2:
                    self.getGame().reset()
                    self.getGame().changeView(Constants.PLAYING_VIEW)