from Game.Views.View import View
from Game.Views.GameOverView import GameOverView
from Game.Views.HighscoresView import HighscoresView
from Game.Views.MenuView import MenuView
from Game.Views.PlayingGameView import PlayingGameView