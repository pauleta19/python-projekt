import pygame
from Game.Views.View import View
from Game import Highscores
from Game.Share import *
class HighscoresView(View):

    def __init__(self, game):
        super(HighscoresView, self).__init__(game)
        self.__highscoresSprite = pygame.image.load(Constants.IMAGE_HIGHSCORES)

    def render(self):
        self.getGame().screen.blit(self.__highscoresSprite, (50, 50))

        self.clearText()

        highscores = Highscores()

        x = 350
        y = 100
        for score in highscores.getScores():
            self.addText(score[0], x, y, size = 28)
            self.addText(str(score[1]), x + 200, y, size = 28)

            y += 30

        self.addText("Nacisnij F2 aby rozpoczac nowa gre", x, y + 60, size = 28)

        super(HighscoresView, self).render()

    def handleEvents(self, events):
        super(HighscoresView, self).handleEvents(events)

        for event in events:
            if event.type == pygame.QUIT:
                exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_F2:
                    self.getGame().reset()
                    self.getGame().changeView(Constants.PLAYING_VIEW)