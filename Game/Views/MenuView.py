import pygame
from Game.Views.View import View
from Game.Share import *

class MenuView(View):

     def __init__(self, game):
         super(MenuView, self).__init__(game)

         self.addText("F2 - Nowa gra", x = 300, y = 200, size = 28)
         self.addText("F3 - Najlepsze wynik", x = 300, y = 240, size = 28)
         self.addText("F4 - Zamknij", x = 300, y = 280, size = 28)

         self.__menuSprite = pygame.image.load(Constants.IMAGE_MENU)

     def render(self):
         self.getGame().screen.blit(self.__menuSprite, (50, 50))

         super(MenuView, self).render()

     def handleEvents(self, events):
         super(MenuView, self).handleEvents(events)

         for event in events:
             if event.type == pygame.QUIT:
                 exit()

             if event.type == pygame.KEYDOWN:
                 if event.key == pygame.K_ESCAPE:
                     exit()

                 if event.key == pygame.K_F2:
                     self.getGame().changeView(Constants.PLAYING_VIEW)

                 if event.key == pygame.K_F3:
                     self.getGame().changeView(Constants.HIGHSCORES_VIEW)

                 if event.key == pygame.K_F4:
                     exit()