from Game.Share import *

class Paddle(GameObject):

    def __init__(self, pos, image):
        super(Paddle, self).__init__(pos, Constants.PADDLE_SIZE, image)

    def setPos(self, pos):

        newPos = [pos[0], pos[1]]
        size = self.getSize()

        if newPos[0] + size[0] > Constants.SCREEN_SIZE[0]:
            newPos[0] = Constants.SCREEN_SIZE[0] - size[0]

        super(Paddle, self).setPos(newPos)