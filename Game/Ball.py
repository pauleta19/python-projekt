import pygame
from Game.Share import *

class Ball(GameObject):

    def __init__(self, pos, image, game):
        self.__game = game
        self.__speed = 3
        self.__increment = [2, 2]
        self.__direction = [1, 1]
        self.__inMotion = 0

        super(Ball, self).__init__(pos, Constants.BALL_SIZE, image)

    def setSpeed(self, newSpeed):
        self.__speed = newSpeed

    def resetSpeed(self):
        self.setSpeed(3)

    def getSpeed(self):
        return self.__speed

    def isInMotion(self):
        return self.__inMotion

    def setMotion(self, isMoving):
        self.__inMotion = isMoving
        self.resetSpeed()

    def changeDirection(self, gameObject):

        pos= self.getPos()
        size = self.getSize()
        objectPos = gameObject.getPos()
        objectSize = gameObject.getSize()

        if pos[1] > objectPos[1] and \
                        pos[1] < objectPos[1] + objectSize[1] and \
                        pos[0] > objectPos[0] and\
                        pos[0] < objectPos[0] + objectSize[0]:
            self.setPos((pos[0], objectPos[1] + objectSize[1]))
            self.__direction[1] *= -1

        elif pos[1] + size[1] > objectPos[1] and \
                                pos[1] + size[1] < objectPos[1] + objectSize[1] and \
                        pos[0] > objectPos[0] and \
                        pos[0] < objectPos[0] + objectSize[0]:
            self.setPos((pos[0], objectPos[1] - objectSize[1]))
            self.__direction[1] *= -1

        elif pos[0] + size[0] > objectPos[0] and \
                                pos[0] + size[0] < objectPos[0] + objectSize[0]:
            self.setPos((objectPos[0] - size[0], pos[1]))
            self.__direction[0] *= -1

        else:
            self.setPos((objectPos[0] + objectSize[0], pos[1]))
            self.__direction[0] *= -1
            self.__direction[1] *= -1

    def updatePos(self):

        if not self.isInMotion():
            padPos = self.__game.getPaddle().getPos()
            self.setPos((
                padPos[0] + (Constants.PADDLE_SIZE[0] / 2),
                Constants.SCREEN_SIZE[1] - Constants.PADDLE_SIZE[1] - Constants.BALL_SIZE[1]
            ))
            return

        pos = self.getPos()
        size = self.getSize()

        newPos = [pos[0] + (self.__increment[0] * self.__speed) * self.__direction[0],
                       pos[1] + (self.__increment[1] * self.__speed) * self.__direction[1]]

        if newPos[0] + size[0] >= Constants.SCREEN_SIZE[0]:
            self.__direction[0] *= -1
            newPos = [Constants.SCREEN_SIZE[0] - size[0], newPos[1]]
            self.__game.playSound(Constants.SOUND_HIT_WALL)

        if newPos[0] <= 0:
            self.__direction[0] *= -1
            newPos = [0, newPos[1]]
            self.__game.playSound(Constants.SOUND_HIT_WALL)

        if newPos[1] + size[1] >= Constants.SCREEN_SIZE[1]:
            self.__direction[1] *= -1
            newPos = [newPos[0], Constants.SCREEN_SIZE[1] - size[1]]

        if newPos[1] <= 0:
            self.__direction[1] *= -1
            newPos = [newPos[0], 0]
            self.__game.playSound(Constants.SOUND_HIT_WALL)

        self.setPos(newPos)

    def isBallDead(self):
        pos = self.getPos()
        size = self.getSize()

        if pos[1] + size[1] >= Constants.SCREEN_SIZE[1]:
            return 1

        return 0
