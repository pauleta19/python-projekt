class GameObject:

    def __init__(self, pos, size, image):
        self.__pos = pos
        self.__size = size
        self.__sprite = image

    def getPos(self):
        return self.__pos

    def setPos(self, pos):
        self.__pos = pos

    def getSize(self):
        return self.__size

    def getImage(self):
        return self.__sprite

    def __intersectsY(self, other):
        otherPos = other.getPos()
        otherSize = other.getSize()

        if self.__pos[1] >= otherPos[1] and self.__pos[1] <= otherPos[1] + otherSize[1]:
            return 1
        if (self.__pos[1] + self.__size[1]) > otherPos[1] and (self.__pos[1] + self.__size[1]) <= (otherPos[1] + otherSize[1]):
            return 1
        return 0

    def __intersectsX(self, other):
        otherPos = other.getPos()
        otherSize = other.getSize()

        if self.__pos[0] >= otherPos[0] and self.__pos[0] <= otherPos[0] + otherSize[0]:
            return 1
        if (self.__pos[0] + self.__size[0]) > otherPos[0] and (self.__pos[0] + self.__size[0]) <= (otherPos[0] + otherSize[0]):
            return 1
        return 0

    def intersects(self, other):
        if self.__intersectsY(other) and self.__intersectsX(other):
            return 1
        return 0
