import os


class Constants:
    SCREEN_SIZE = [800, 600]
    BRICK_SIZE = [100, 30]
    BALL_SIZE = [16, 16]
    PADDLE_SIZE = [150, 38]


    IMAGE_BALL = os.path.join("Game", "Resources", "ball.png")
    IMAGE_PADDLE = os.path.join("Game", "Resources", "paddle.png")
    IMAGE_BRICK = os.path.join("Game", "Resources", "normal.png")
    IMAGE_HIGHSCORES = os.path.join("Game", "Resources", "highscores.png")
    IMAGE_MENU = os.path.join("Game", "Resources", "menu.png")
    IMAGE_SPEED_UP_BRICK = os.path.join("Game", "Resources", "speed.png")
    IMAGE_LIFE_UP_BRICK = os.path.join("Game", "Resources", "life.png")
    IMAGE_DOUBLE_HIT_BRICK = os.path.join("Game", "Resources", "double.png")

    SOUND_FILE_HIT_BRICK = os.path.join("Game", "Resources", "Hit.wav")
    SOUND_FILE_HIT_WALL = os.path.join("Game", "Resources", "Bounce.wav")
    SOUND_FILE_HIT_PADDLE = os.path.join("Game", "Resources", "PaddleBounce.wav")
    SOUND_FILE_GAMEOVER = os.path.join("Game", "Resources", "GameOver.wav")
    SOUND_FILE_HIT_BRICK_ADD_LIFE = os.path.join("Game", "Resources", "AddLife.wav")
    SOUND_FILE_HIT_BRICK_SPEED_UP = os.path.join("Game", "Resources", "SpeedUp.wav")

    SOUND_GAMEOVER = 0
    SOUND_HIT_BRICK = 1
    SOUND_HIT_BRICK_ADD_LIFE = 2
    SOUND_HIT_BRICK_SPEED_UP = 3
    SOUND_HIT_WALL = 4
    SOUND_HIT_PADDLE = 5

    PLAYING_VIEW = 0
    GAMEOVER_VIEW = 1
    HIGHSCORES_VIEW = 2
    MENU_VIEW = 3

