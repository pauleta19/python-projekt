import pygame

from Game import *
from Game.Views import *
from Game.Share import Constants

class CatBreakout:

    def __init__(self):
        self.__lives = 1
        self.__score = 0

        self.__level = Level(self)
        self.__level.load(0)

        self.__paddle = Paddle((Constants.SCREEN_SIZE[0] / 2,
                                Constants.SCREEN_SIZE[1] - Constants.PADDLE_SIZE[1]),
                               pygame.image.load(Constants.IMAGE_PADDLE)
                               )
        self.__balls = [
            Ball((400, 400), pygame.image.load(Constants.IMAGE_BALL), self)
        ]

        pygame.init()
        pygame.mixer.init()
        pygame.display.set_caption("Cat Breakout Project")

        self.__clock = pygame.time.Clock()

        self.screen = pygame.display.set_mode(Constants.SCREEN_SIZE,
                                              pygame.DOUBLEBUF, 32)

        pygame.mouse.set_visible(0)

        self.__views = (
            PlayingGameView(self),
            GameOverView(self),
            HighscoresView(self),
            MenuView(self)
        )

        self.__currentView = 3

        self.__sounds = (
            pygame.mixer.Sound(Constants.SOUND_FILE_GAMEOVER),
            pygame.mixer.Sound(Constants.SOUND_FILE_HIT_BRICK),
            pygame.mixer.Sound(Constants.SOUND_FILE_HIT_BRICK_ADD_LIFE),
            pygame.mixer.Sound(Constants.SOUND_FILE_HIT_BRICK_SPEED_UP),
            pygame.mixer.Sound(Constants.SOUND_FILE_HIT_WALL),
            pygame.mixer.Sound(Constants.SOUND_FILE_HIT_PADDLE)
        )

    def start(self):
        while 1:
            self.__clock.tick(100)

            self.screen.fill((0, 0, 0))

            currentView = self.__views[self.__currentView]
            currentView.handleEvents(pygame.event.get())
            currentView.render()

            pygame.display.update()

    def changeView(self, view):
        self.__currentView = view

    def getLevel(self):
        return self.__level

    def getScore(self):
        return self.__score

    def increaseScore(self, score):
        self.__score += score

    def getLives(self):
        return self.__lives

    def getBalls(self):
        return self.__balls

    def getPaddle(self):
        return self.__paddle

    def playSound(self, soundClip):
        sound = self.__sounds[soundClip]

        sound.stop()
        sound.play()

    def reduceLives(self):
        self.__lives -= 1

    def increaseLives(self):
        self.__lives += 1

    def reset(self):
        self.__lives = 1
        self.__score = 0
        self.__level.load(0)

if __name__ == "__main__":
	CatBreakout().start()
